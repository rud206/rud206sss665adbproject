package nyu.edu.adb.recpec;

/**
 * @author Rahul Desai, Suruchi Sharma Class to hold the historical values for a
 *         variable
 *
 */
public class VariableHistory {
  private int htime;
  private int value;

  public VariableHistory(int htime, int value) {
    this.htime = htime;
    this.value = value;
  }

  public int getTime() {
    return htime;
  }

  public int getValue() {
    return value;
  }

  public void setTime(int htime) {
    this.htime = htime;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return ("|" + this.htime + "," + this.value + "|");
  }
}